#!/bin/bash
sudo apt install ranger
echo "VISUAL=vim; export VISUAL EDITOR=vim; export EDITOR" >> ~/.bashrc
echo "alias ranger='TERM=mate-terminal ranger'" >> ~/.bashrc
