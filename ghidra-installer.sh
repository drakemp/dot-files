
apt install openjdk-11-jdk

GHIDRA="$HOME/ghidra"
mkdir $GHIDRA
wget https://ghidra-sre.org/ghidra_9.0_PUBLIC_20190228.zip -O $GHIDRA/ghidra_9.0.zip
(cd $GHIDRA; unzip ghidra_9.0.zip)
echo 'export PATH="$HOME/ghidra/ghidra_9.0/:$PATH"' >> $HOME/.bashrc
echo 'To open Ghidra:'
echo '\t$ ghidraRun'
echo 'Restart your terminal now!!!'
