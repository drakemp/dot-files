#!/bin/bash

# python env
sudo apt-get install -y git zsh make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev tree jq
curl -sL https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
if [ `echo $?` -eq 0 ]; then
   echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> ~/.bashrc
   echo 'eval "$(pyenv init -)"' >> ~/.bashrc
   echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc

   export PATH="$HOME/.pyenv/bin:$PATH"
   eval "$(pyenv init -)"
   eval "$(pyenv virtualenv-init -)"

   sudo apt-get install python3-setuptools
   sudo apt-get install libffi-dev
   pyenv install 3.7.1
   pyenv global 3.7.1
else
   echo "There was an error in the pyenv-installer"
fi

