#!/bin/bash
source ~/.bashrc

sudo apt install i3 i3status suckless-tools feh

echo "[-] Backup of original settings are found in ~/.config/mate.dconf.bak"
echo "[!] Comment 'dconf dump ...', if running more than once!!!"
echo "[-] to restore '$ dconf load /org/mate/ < ~/.config/mate.bak'"
dconf dump /org/mate/ > ~/.config/mate.bak

dconf write /org/mate/desktop/session/required-components/windowmanager "'i3'"
#caja just breaks stuff, install ranger instead
dconf write /org/mate/desktop/session/required-components/filemanager "'ranger'"
dconf write /org/mate/desktop/session/required-components-list "['windowmanager','dock']"

#copy i3 config here
cp ./i3-config ~/.config/i3/config

#install i3-alternating-layout here
sudo apt-get install x11-utils python-pip git
pip install i3-py
git clone https://github.com/olemartinorg/i3-alternating-layout ~/.config/i3/i3-alternating-layout

echo "[!] Logout required"
echo "[*] Happy ricing!"
