#!/bin/bash

apt-get remove radare2 radare2-lib
apt-get purge radare2 radare2-lib

git clone https://github.com/radare/radare2.git ~/radare2

( cd ~/radare2
  sys/install.sh
)
